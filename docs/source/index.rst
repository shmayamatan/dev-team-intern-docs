.. Dev Team Interns documentation master file, created by
   sphinx-quickstart on Sun Nov 25 10:13:23 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Dev Team Interns's program!
============================================

This program will guide you through your first steps as a developer.
We will develop your **self learning** skills, your **problem solving** skills, and your knowledge in the web development field.

Oh, you will also learn some **CYBER, WOW!**

Program Structure
^^^^^^^^^^^^^^^^^

The program is seperated into different **sections**. Each section represents a different topic.

Every topic will have the following **titles**:
    * **Key Goals** - A short description of the main objectives.
    * **Content** - What is actually going to be learned.
    * **Useful Links** - A list of some useful resources. - *(optional)*
    * **Exercises** - A practical exercise to practice what you just learned. - *(optional)*

.. toctree::
   :maxdepth: 1
   :caption: Getting started

   tutorial/howto

.. toctree::
   :maxdepth: 1
   :caption: Guide

   license
   help

